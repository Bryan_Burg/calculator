import 'package:flutter/material.dart';
import 'functios/therme.dart';

class Home2 extends StatefulWidget {
  const Home2({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<Home2> createState() => _HomeState();
}

class _HomeState extends State<Home2> {
  final TextEditingController _fController = TextEditingController();
  final TextEditingController _cController = TextEditingController();
  final TextEditingController _kController = TextEditingController();

  double _f = double.parse((0).toStringAsFixed(2));
  double _c = double.parse((0).toStringAsFixed(2));
  double _k = double.parse((0).toStringAsFixed(2));

  final thermo _thermo = thermo();

  @override
  void dispose() {
    _fController.dispose();
    _cController.dispose();
    _kController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 155, 15, 215),
                ),
                child: Text(
                  'Calculadora de Temperatura',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 30,
                  ),
                ),
              ),
              ListTile(
                title: const Text('Calculadora'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/calculadora');
                },
              ),
              ListTile(
                title: const Text('Sistema de mediciones'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.pushNamed(context, '/sism');
                },
              ),
            ],
          ),
        ),
        body: Column(
          children: [
            Expanded(
                child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Column(
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            const Text(
                              "Grados Fahrenheit",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.all(20),
                              child: TextField(
                                controller: _fController,
                                keyboardType: TextInputType.number,
                                onChanged: (value) {
                                  setState(() {
                                    _f = double.tryParse(value) ?? 0.0;
                                    _c = _thermo.fahrenheit_c(_f);
                                    _k = _thermo.fahrenheit_k(_f);
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            const Text(
                              "Grados Celsius",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.all(20),
                              child: TextField(
                                controller: _cController,
                                keyboardType: TextInputType.number,
                                onChanged: (value) {
                                  setState(() {
                                    _c = double.tryParse(value) ?? 0.0;
                                    _f = _thermo.celsius_f(_c);
                                    _k = _thermo.celsius_k(_c);
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            const Text(
                              "Grados Kelvin",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.all(20),
                              child: TextField(
                                controller: _kController,
                                keyboardType: TextInputType.number,
                                onChanged: (value) {
                                  setState(() {
                                    _k = double.tryParse(value) ?? 0.0;
                                    _f = _thermo.kelvin_f(_k);
                                    _c = _thermo.kelvin_c(_k);
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Expanded(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              textAlign: TextAlign.center,
                              "$_f ºF",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                              ),
                            )
                          ],
                        )),
                        Expanded(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              textAlign: TextAlign.center,
                              "$_c ºC",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                              ),
                            )
                          ],
                        )),
                        Expanded(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              textAlign: TextAlign.center,
                              "$_k ºK",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                              ),
                            )
                          ],
                        )),
                      ],
                    )),
              ],
            ))
          ],
        ));
  }
}
