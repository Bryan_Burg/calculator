import 'dart:math';

import 'package:calculadora_2/functios/therme.dart';
import 'package:flutter/material.dart';

class liquido extends StatefulWidget {
  const liquido({Key? key}) : super(key: key);

  @override
  _LiquidConverterState createState() => _LiquidConverterState();
}

class _LiquidConverterState extends State<liquido> {
  final TextEditingController _gallonsController = TextEditingController();
  final TextEditingController _fluidOuncesController = TextEditingController();
  double _litersFromGallons = 0;
  double _litersFromFluidOunces = 0;

  @override
  void dispose() {
    _gallonsController.dispose();
    _fluidOuncesController.dispose();
    super.dispose();
  }

  void _convertFromGallons() {
    setState(() {
      double gallons = double.tryParse(_gallonsController.text) ?? 0;
      double totalFluidOunces = gallons * 128;
      _litersFromGallons = totalFluidOunces / 33.814;
    });
  }

  void _convertFromFluidOunces() {
    setState(() {
      double fluidOunces = double.tryParse(_fluidOuncesController.text) ?? 0;
      _litersFromFluidOunces = fluidOunces / 33.814;
    });
  }

  double get _gramsFromLitersGallons {
    return _litersFromGallons * 1000;
  }

  double get _gramsFromLitersFluidOunces {
    return _litersFromFluidOunces * 1000;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Medidor de Liquidos'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextField(
                controller: _gallonsController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Galones',
                  hintText: 'Valor en Galones', 
                ),
              ),
              TextButton(
                onPressed: _convertFromGallons,
                child: Text('Convertir a litros', style: TextStyle(fontSize: 15,color: Color.fromARGB(255, 19, 170, 216))),
              ),
              // SizedBox(height: 16.0),
              Text(
                '$_litersFromGallons Lt = ${_gramsFromLitersGallons.toStringAsFixed(2)} g',
                style: TextStyle(fontSize: 24.0),
              ),
              // SizedBox(height: 32.0),
              TextField(
                controller: _fluidOuncesController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Onzas liquidas',
                  hintText: 'Valor de onzas liquidas',
                ),
              ),
              TextButton(
                onPressed: _convertFromFluidOunces,
                child: Text('Convertir a litros',  style: TextStyle(fontSize: 15,color: Color.fromARGB(255, 19, 170, 216))),
              ),
              // SizedBox(height: 16.0),
              Text(
                '$_litersFromFluidOunces lt = ${_gramsFromLitersFluidOunces.toStringAsFixed(2)} g',
                style: TextStyle(fontSize: 24.0),
              ),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.calculate),
              label: 'Longitud',
            ),
            BottomNavigationBarItem(
              icon: Icon( Icons.fiber_smart_record, ),
              label: 'Masa',
            ),
            BottomNavigationBarItem(
              icon: Icon( Icons.water_drop, ),
              label: 'Capacidad',
            ),
          ],
          onTap: (int index) {
            switch (index) {
              case 0:
                Navigator.pushNamed(context, '/liqu');
                break;
              case 1:
                Navigator.pushNamed(context, '/masa');
                break;
              case 2:
                Navigator.pushNamed(context, '/liqu');
                break;
            }
          },
        ));
  }
}
