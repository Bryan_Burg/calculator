import 'package:flutter/material.dart';

class Masa extends StatefulWidget {
  const Masa({Key? key}) : super(key: key);

  @override
  _WeightConverterState createState() => _WeightConverterState();
}

class _WeightConverterState extends State<Masa> {
  final TextEditingController _ouncesController = TextEditingController();
  final TextEditingController _poundsController = TextEditingController();
  double _gramsFromOunces = 0;
  double _gramsFromPounds = 0;

  @override
  void dispose() {
    _ouncesController.dispose();
    _poundsController.dispose();
    super.dispose();
  }

  void _convertFromOunces() {
    setState(() {
      double ounces = double.tryParse(_ouncesController.text) ?? 0;
      _gramsFromOunces = ounces * 28.35;
    });
  }

  void _convertFromPounds() {
    setState(() {
      double pounds = double.tryParse(_poundsController.text) ?? 0;
      _gramsFromPounds = pounds * 453.59;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Calculadora de masa'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextField(
                controller: _ouncesController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Onzas',
                  hintText: 'Valor en onzas',
                ),
                onChanged: (_) => _convertFromOunces(),
              ),
              SizedBox(height: 16.0),
              TextField(
                controller: _poundsController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Libras',
                  hintText: 'Valor en libras',
                ),
                onChanged: (_) => _convertFromPounds(),
              ),
              SizedBox(height: 32.0),
              Text(
                '${_ouncesController.text} Onzas = $_gramsFromOunces g.',
                style: TextStyle(fontSize: 20.0),
              ),
              SizedBox(height: 16.0),
              Text(
                '${_poundsController.text} Libras  = $_gramsFromPounds g.',
                style: TextStyle(fontSize: 20.0),
              ),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.calculate_rounded),
              label: 'Longitud',
            ),
             BottomNavigationBarItem(
              icon: Icon( Icons.fiber_smart_record, ),
              label: 'Masa',
            ),
            BottomNavigationBarItem(
              icon: Icon( Icons.water_drop, ),
              label: 'Capacidad',
            ),
          ],
          onTap: (int index) {
            switch (index) {
              case 0:
                Navigator.pushNamed(context, '/liqu');
                break;
              case 1:
                Navigator.pushNamed(context, '/masa');
                break;
              case 2:
                Navigator.pushNamed(context, '/liqu');
                break;
            }
          },
        ));
  }
}
