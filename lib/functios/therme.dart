import 'package:flutter/material.dart';

class therme1{

  pulgadas(pulgadas){
    double cm = pulgadas * 2.54;
    return cm;
  }
  yarda(yarda){
    double cm = yarda * 91.44;
    return cm;

  }
  pie(pie){
    double cm = pie * 30.48;    
    return cm;

  }
  milla(milla){
    double km = milla * 1.60934;    
    return km;

  }
 
  milla_nautica(milla_nautica){
    double km = milla_nautica * 1.8519984;
    return km;
  }
 
}


class thermo {
  fahrenheit_c(f) {
    double f_c = (f - 32) / 1.8;
    return f_c;
  }

  fahrenheit_k(f) {
    double f_k = (f - 32) * 5 / 9 + 273.15;
    return f_k;
  }

  celsius_f(c) {
    double c_f = (c * 1.8) + 32;
    return c_f;
  }

  celsius_k(c) {
    double c_k = c + 273.15;
    return c_k;
  }

  kelvin_c(k) {
    double k_c = (k - 273.15) * 9 / 5 + 32;
    return k_c;
  }

  kelvin_f(k) {
    double k_f = k - 273.15;
    return k_f;
  }
}
