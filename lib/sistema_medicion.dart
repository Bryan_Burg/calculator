import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'functios/therme.dart';

class Sism extends StatefulWidget {
  const Sism({super.key, required this.title});

  final String title;

  @override
  State<Sism> createState() => _Sism();
}

class _Sism extends State<Sism> {
  final TextEditingController pulgadas = TextEditingController();
  final TextEditingController pies = TextEditingController();
  final TextEditingController yardas = TextEditingController();
  final TextEditingController millas = TextEditingController();
  final TextEditingController millas_nautica = TextEditingController();
  double cmd = 0.0; // Nueva variable agregada
  double cmp = 0.0; // Nueva variable agregada
  double cmy = 0.0; // Nueva variable agregada
  double km = 0.0;
  double kmm = 0.0;

  therme1 Therme = new therme1();
  @override
  void dispose() {
    pulgadas.dispose();
    pies.dispose();
    yardas.dispose();
    millas.dispose();
    millas_nautica.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sistema de Mediciones"),
      ),
      body: Column(
        children: [
          Expanded(
              child: Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Column(
                    children: [
                      Expanded(
                          child: Column(
                        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const Text("Pulgada"),
                          Container(
                              // margin: EdgeInsets.all(10),
                              child: TextField(
                            controller: pulgadas,
                            keyboardType:
                                TextInputType.numberWithOptions(decimal: true),
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                  RegExp(r'^\d+\.?\d{0,1}'))
                            ],
                          )),
                        ],
                      )),
                      Expanded(
                          child: Column(
                        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const Text("Pie"),
                          Container(
                            // margin: EdgeInsets.all(10),
                            child: TextField(
                              controller: pies,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ],
                      )),
                      Expanded(
                          child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text("Yarda"),
                          Container(
                            // margin: EdgeInsets.all(10),
                            child: TextField(
                              controller: yardas,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ],
                      )),
                      Expanded(
                          child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text("Milla"),
                          Container(
                            // margin: EdgeInsets.all(10),
                            child: TextField(
                              controller: millas,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ],
                      )),
                      Expanded(
                          child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text("Milla Nautica"),
                          Container(
                            // margin: EdgeInsets.all(10),
                            child: TextField(
                              controller: millas_nautica,
                              keyboardType: TextInputType.number,
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
              Expanded(
                  flex: 0,
                  child: Column(
                    children: [
                      Padding(padding: EdgeInsets.all(5)),
                      TextButton(
                          onPressed: () {
                            double Pulgada =
                                double.tryParse(pulgadas.text) ?? 0.0;
                            double yarda = double.tryParse(yardas.text) ?? 0.0;
                            double pie = double.tryParse(pies.text) ?? 0.0;
                            double milla = double.tryParse(millas.text) ?? 0.0;
                            double milla_nautica =
                                double.tryParse(millas_nautica.text) ?? 0.0;
                            setState(() {
                              cmd = Therme.pulgadas(Pulgada) as double;
                              cmp = Therme.pie(yarda) as double;
                              cmy = Therme.yarda(pie) as double;
                              km = Therme.milla(milla) as double;
                              kmm =
                                  Therme.milla_nautica(milla_nautica) as double;
                            });
                          },
                          child: Text("Calcular")),
                      Expanded(
                          child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [Text("Pu $cmd CM")],
                      )),
                      Expanded(
                          child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [Text("Y $cmy CM")],
                      )),
                      Expanded(
                          child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [Text("Pi $cmp CM")],
                      )),
                      Expanded(
                          child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [Text("M: $km KM")],
                      )),
                      Expanded(
                          child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [Text("Mn: $kmm KM")],
                      )),
                    ],
                  )),
            ],
          ))
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.calculate),
            label: 'Longitud',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.fiber_smart_record,
            ),
            label: 'Masa',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.water_drop,
            ),
            label: 'Capacidad',
          ),
        ],
        onTap: (int index) {
          switch (index) {
            case 0:
              Navigator.pushNamed(context, '/liqu');
              break;
            case 1:
              Navigator.pushNamed(context, '/masa');
              break;
            case 2:
              Navigator.pushNamed(context, '/liqu');
              break;
          }
        },
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.orange,
              ),
              child: Text(
                'Sistema de mediciones',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
            ListTile(
              title: const Text('Calculadora'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/calculadora');
              },
            ),
            ListTile(
              title: const Text('Calculadora de Temperatura'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/temp');
              },
            ),
          ],
        ),
      ),
    );
  }
}
