// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors, library_private_types_in_public_api

import 'package:calculadora_2/liquido.dart';
import 'package:calculadora_2/masa.dart';
import 'package:calculadora_2/sistema_medicion.dart';
import 'package:calculadora_2/temperaturas.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      debugShowCheckedModeBanner: false,
      home: Calculator(),
      themeMode: ThemeMode.system,
      routes: {
        '/calculadora': (context) => Calculator(),
        '/temp': (context) => Home2(title: 'Temperatura'),
        '/sism': (context) => Sism(title: 'sims'),
        '/masa': (context) => Masa(),
        '/liqu': (context) => liquido(),
      },
    );
  }
}

class Calculator extends StatefulWidget {
  @override
  _CalculatorState createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {
  dynamic displaytxt = 20;
  //este es el Widget del boton
  Widget calcbutton(String btntxt, Color btncolor, Color txtcolor) {
    return Container(
      child: TextButton(
        onPressed: () {
          calculate(btntxt);
        },
        child: Text(
          '$btntxt',
          style: TextStyle(
            fontSize: 25,
            color: txtcolor,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    //Calculator
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text(
          'Calculadora',
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.all(12),
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blueGrey,
              ),
              child: Text(
                'Calculadora',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
            ),
            ListTile(
              title: const Text('Calculadora de Temperatura'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/temp');
              },
            ),
            ListTile(
              title: const Text('Calculadora de mediciones'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/sism');
              },
            ),
          ],
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: (Alignment.bottomCenter),
                colors: [Colors.white, Colors.white])),
        padding: EdgeInsets.symmetric(horizontal: 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            // Calculator display
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 200),
                      child: Column(
                        children: [
                          Text(
                            '$text',
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 60,
                            ),
                          ),
                        ],
                      )),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                calcbutton(
                    'AC', Colors.red, Colors.red),
                // calcbutton('+/-', Colors.black, Colors.black),
                calcbutton('<', Colors.black, Colors.black),
                calcbutton('/', Colors.black, Colors.black),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                calcbutton('7', Colors.black, Colors.black),
                calcbutton('8', Colors.black, Colors.black),
                calcbutton('9', Colors.black, Colors.black),
                calcbutton('x', Colors.black, Colors.black),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                calcbutton('4', Colors.black, Colors.black),
                calcbutton('5', Colors.black, Colors.black),
                calcbutton('6', Colors.black, Colors.black),
                calcbutton('-', Colors.black, Colors.black),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                calcbutton('1', Colors.black, Colors.black),
                calcbutton('2', Colors.black, Colors.black),
                calcbutton('3', Colors.black, Colors.black),
                calcbutton('+', Colors.black, Colors.black),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                //this is button Zero
                TextButton(
                  onPressed: () {
                    calculate('0');
                  },
                  child: Text(
                    '0',
                    style: TextStyle(fontSize: 25, color: Colors.black),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    calculate('00');
                  },
                  child: Text(
                    '00',
                    style: TextStyle(fontSize: 25, color: Colors.black),
                  ),
                ),
                calcbutton('.', Color.fromARGB(255, 0, 0, 0), Colors.black),
                calcbutton('=', Color.fromARGB(255, 0, 0, 0), Color.fromARGB(255, 0, 0, 0)),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

// Logic
  double firstNumber = 0;
  double secondNumber = 0;
  String result = "";
  String text = "0";
  String operation = "";

  void calculate(String btnText) {
    if (btnText == "AC") {
      result = "0";
      text = "0";
      firstNumber = 0;
      secondNumber = 0;
    } else if (btnText == "+" ||
        btnText == "-" ||
        btnText == "x" ||
        btnText == "/") {
      firstNumber = double.parse(text);
      result = "";
      operation = btnText;
    } else if (btnText == '+/-') {
      if (text[0] != '-') {
        result = '-' + text;
      } else {
        result = text.substring(1);
      }
    } else if (btnText == '.') {
      if (!result.toString().contains('.')) {
        result = result.toString() + '.';
      }
    } else if (btnText == '<') {
      result = text.substring(0, text.length - 1);
    } else if (btnText == "=") {
      secondNumber = double.parse(text);
      if (operation == "+") {
        result = (firstNumber + secondNumber).toString();
      }
      if (operation == "-") {
        result = (firstNumber - secondNumber).toString();
      }
      if (operation == "x") {
        result = (firstNumber * secondNumber).toString();
      }
      if (operation == "/") {
        result = (firstNumber / secondNumber).toString();
      }
    } else {
      result = double.parse(text + btnText).toString();
    }
    setState(() {
      text = result;
    });
  }
}
